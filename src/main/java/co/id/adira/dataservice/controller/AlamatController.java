/**
 * 
 */
package co.id.adira.dataservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.id.adira.dataservice.domain.Alamat;
import co.id.adira.dataservice.util.ApiResponse;
import co.id.adira.dataservice.util.VueUtil;
import co.id.adira.dataservice.repository.AlamatRepository;

/**
 * @author fatchurrachman
 *
 */
@RefreshScope
@RestController
@RequestMapping("/api/v1")
public class AlamatController extends MainController {
	
	private static final long serialVersionUID = 2950776365989939031L;

	@Autowired
	private AlamatRepository alamatRepository;
	
	@GetMapping(path = "/alamat", produces = "application/json")
	public ResponseEntity<ApiResponse<List<Alamat>>> list() {
		int totalRec = (int) alamatRepository.count();
		
		VueUtil vue = new VueUtil();
		vue.setCurrentPage(1);
		vue.setPerPage(perPage);
		vue.setFrom(1);
		vue.setTo(10);
		vue.setTotal(totalRec);
		vue.setFirstPage(firstPage);
		vue.setLastPage("test-last-page");
		vue.setNextPage("test-next-page");
		
		return ResponseEntity.ok(new ApiResponse<>(vue.getPage(), vue.getLinks(), alamatRepository.findAll()));
	}

}
