package co.id.adira.dataservice.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author fatchurrachman
 *
 */
@RefreshScope
public class MainController implements Serializable {

	private static final long serialVersionUID = -3501279700088165637L;
	
	@Value("${constants.url}")
	protected String firstPage;
	
	@Value("${constants.per-page}")
	protected int perPage;

}
