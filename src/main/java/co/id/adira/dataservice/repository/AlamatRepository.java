package co.id.adira.dataservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.adira.dataservice.domain.Alamat;

@Repository
public interface AlamatRepository extends CrudRepository<Alamat, Long> {}