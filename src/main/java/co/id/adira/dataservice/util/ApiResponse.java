/**
 * 
 */
package co.id.adira.dataservice.util;

import java.util.Map;
import lombok.Data;

/**
 * @author fatchurrachman
 *
 */
@Data
public class ApiResponse<T> {
	
	private Map<String, Map<String, Integer>> meta;
	private Map<String, String> links;
    private Object data;
    
    public ApiResponse(Map<String, Map<String, Integer>> meta, Map<String, String> links, Object data) {
    	this.meta  = meta;
        this.links = links;
        this.data  = data;
    }

}