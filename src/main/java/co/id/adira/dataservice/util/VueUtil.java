package co.id.adira.dataservice.util;

import java.util.HashMap;
import java.util.Map;

public class VueUtil {
	
	private String firstPage;
	private String lastPage;
	private String nextPage;
	private String prevPage;
	private int currentPage;
	private int perPage;
	private int from;
	private int to;
	private int total;
	private Map<String, String> links;
	private Map<String, Map<String, Integer>> page;
	
	public String getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(String firstPage) {
		this.firstPage = firstPage;
		this.addLink(Constants.COMMON.FIRST, firstPage);
	}

	public String getLastPage() {
		return lastPage;
	}

	public void setLastPage(String lastPage) {
		this.lastPage = lastPage;
		this.addLink(Constants.COMMON.LAST, lastPage);
	}

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
		this.addLink(Constants.COMMON.NEXT, nextPage);
	}

	public String getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(String prevPage) {
		this.prevPage = prevPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Map<String, String> getLinks() {
		return links;
	}

	public void setLinks(Map<String, String> links) {
		this.links = links;
	}

	public Map<String, Map<String, Integer>> getPage() {
		if (null == page) {
			page = new HashMap<String, Map<String,Integer>>();
		}
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put(Constants.COMMON.FROM, getFrom());
		map.put(Constants.COMMON.TO, getTo());
		map.put(Constants.COMMON.TOTAL, getTotal());
		map.put(Constants.COMMON.CURRENT_PAGE, getCurrentPage());
		map.put(Constants.COMMON.PER_PAGE, getPerPage());
		
		page.put(Constants.COMMON.PAGE, map);
		return page;
	}

	public void setPage(Map<String, Map<String, Integer>> page) {
		this.page = page;
	}

	/**
	 * add the link to be transform.
	 * @param key the key value
	 * @param value the string
	 */
	public void addLink(String key, String value) {
		if (null == links) {
			links = new HashMap<String, String>();
		}
		links.put(key, value);
	}

}
