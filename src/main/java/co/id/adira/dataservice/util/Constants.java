package co.id.adira.dataservice.util;

public class Constants {
	public static class COMMON {
		public static final String FIRST = "first";
		public static final String LAST = "last";
		public static final String NEXT = "next";
		public static final String FROM = "from";
		public static final String TO = "to";
		public static final String TOTAL = "total";
		public static final String CURRENT_PAGE = "current-page";
		public static final String PER_PAGE = "per-page";
		public static final String PAGE = "page";
	}

}
