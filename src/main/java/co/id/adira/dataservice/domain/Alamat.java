/**
 * 
 */
package co.id.adira.dataservice.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * @author fatchurrachman
 *
 */
@Data
@Entity
@Table(name = "alamats")
public class Alamat implements Serializable {

	private static final long serialVersionUID = 6893320570753649916L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "desa_id")
	private int desaId;
	
	@Column(name = "jalan")
	private String jalan;
	
	@Column(name = "rt")
	private String rt;
	
	@Column(name = "rw")
	private String rw;
	
	@Column(name = "is_alamat_domisili")
	private int isALamatDomisili;
	
	@Column(name = "is_alamat_surat")
	private int isAlamatSurat;
	
	@Column(name = "is_alamat_ktp")
	private int isAlamatKtp;
	
	@Column(name = "deleted_at")
	@Temporal(TemporalType.DATE)
	private Date deletedAt;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	@Column(name = "updated_at")
	@Temporal(TemporalType.DATE)
	private Date updatedAt;
	
	@ManyToOne
    @JoinColumn(name="profile_id")
	private Profile profile;

}
