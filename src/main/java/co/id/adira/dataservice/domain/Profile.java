/**
 * 
 */
package co.id.adira.dataservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author fatchurrachman
 *
 */
@Data
@Entity
@Table(name = "profiles")
public class Profile implements Serializable {

	private static final long serialVersionUID = -3679612945997818922L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nama")
	private String nama;

}