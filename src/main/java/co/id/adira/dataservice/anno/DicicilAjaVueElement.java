package co.id.adira.dataservice.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@RefreshScope
public @interface DicicilAjaVueElement {
	
	@Value("${constants.first}")
	public String first();
}
